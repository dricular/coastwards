# coastwards.org

COASTWARDS is a Citizen Science project initally funded by the Cluster of Excellence 'The Future Ocean', the University of Kiel and the Coastal Risks and Sea-level Rise Research Group.

Participants can help Science study the risks of sea-level rise by uploading pictures of coasts. 

Have any pictures of coasts? Go check it out!

Development has momentarily paused due to lack of funding. I hope to pick up the work again soon!
